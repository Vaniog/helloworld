#pragma once
#include <iostream>

void Draw() {
    uint32_t width = 20, height = 10;
    for (uint32_t y = 0; y <= height; y++) {
        for (uint32_t x = 0; x <= width; x++) {
            if ((x - 10) * (x - 10) + (y - 5) * (y - 5) <= 26)
                std::cout << "#";
            else
                std::cout << " ";
        }
        std::cout << "\n";
    }
}