cmake_minimum_required(VERSION 3.23)
project(helloworld)

set(CMAKE_CXX_STANDARD 14)

include_directories(hellosubmodule)

add_executable(helloworld
        hellosubmodule/writer.h
        main.cpp
        Drawer.h)
